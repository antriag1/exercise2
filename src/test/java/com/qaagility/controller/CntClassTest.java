package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;


public class CntClassTest {

@Test
public void testCntOne() {
    CntClass test = new CntClass();
        assertEquals(test.someAction(10,5), 2);
    }

    @Test
public void testCntTwo() {
    CntClass test = new CntClass();
        assertEquals(test.someAction(10,0), 2147483647);
    }

}
